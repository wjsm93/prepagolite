/* ----------------------- */
/*         Flags           */
/* ----------------------- */

menuToggled = false;

/* ----------------------- */
/*        Métodos          */
/* ----------------------- */

function toggleMenu() {
    if (!menuToggled) {
        $(".menu").css("display","block");
        $(".side-menu").css("display","block");
        setTimeout(function () {
            $(".menu").addClass("faded");
            $(".side-menu").addClass("faded");
        }, 200);
        menuToggled = true;
    } else {
        $(".menu").removeClass("faded");
        $(".side-menu").removeClass("faded");
        setTimeout(function () {
            $(".menu").css("display","none");
            $(".side-menu").css("display","none");
        }, 200);
        menuToggled = false;
    }
}